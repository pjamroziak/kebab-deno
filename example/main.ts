import { Application, ApplicationBuilder } from '../src/core/builders/appplication.builder.ts';
import { Controller, Service } from '../src/core/decorators.ts';
import { IDisposable, IMiddleware } from '../src/core/interfaces.ts';
import { Get } from '../src/http/methods.decorator.ts';
import type { Context } from '../src/http/types.ts';
import { ILogger } from '../src/logger/interfaces.ts';
import { LoggerService } from '../src/logger/logger.service.ts';

@Service()
class HelloWorldService2 {
    public sayHello(): string {
        return 'Hello, World!';
    }
}

@Service()
class HelloWorldService implements IDisposable {
    dispose(): void | Promise<void> {
        console.log('Dispose');
    }
    public sayHello(): string {
        return 'Hello, World!';
    }
}

@Service()
class TestGuard implements IMiddleware, IDisposable {
    dispose(): void {
        console.log('Dispose TestGuard');
    }

    async handle(req: Context): Promise<void> {
        console.log('TestGuard');
    }
}

@Controller({
    path: 'hello-world',
    middlewares: [TestGuard],
})
class HelloWorldController {
    private readonly _logger: ILogger;
    private readonly _helloWorldService: HelloWorldService;

    constructor(helloWorldService: HelloWorldService, loggerService: LoggerService) {
        this._helloWorldService = helloWorldService;
        this._logger = loggerService.getLogger(HelloWorldController.name);
    }

    @Get('/test/:iod')
    getHello(ctx: Context): string {
        return 'Hello, World!';
    }

    @Get('new/*')
    getHello2(): void {
        return;
    }
}

const app = Application.create((builder: ApplicationBuilder) => {
    builder.services.addScoped(HelloWorldService2);
    builder.services.addTransient(HelloWorldService);
    builder.services.addScoped(TestGuard);
    builder.controllers.addController(HelloWorldController);

    builder.useLoggerService(LoggerService);
});

app.listen(3001);
