import { IMiddleware } from './interfaces.ts';

type Type<T extends object> = new (...args: any[]) => T;
type Enum<T> = T[keyof T];

type ControllerOptions = {
    path?: string;
    middlewares?: Type<IMiddleware>[];
};

export type { Type, Enum, ControllerOptions };
