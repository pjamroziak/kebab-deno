import { Context } from '../http/types.ts';

interface IBuilder<T> {
    build(...args: unknown[]): T;
}

interface IMiddleware {
    handle(req: Context): void;
}

interface IDisposable {
    dispose(): void | Promise<void>;
}

export type { IBuilder, IMiddleware, IDisposable };
