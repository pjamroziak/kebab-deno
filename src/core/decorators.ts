import { setMetadata } from '../utils/metadata.ts';
import { CoreDecorator } from './enums.ts';
import { ControllerOptions } from './types.ts';

const Service = () => (target: any) => setMetadata(target, CoreDecorator.Service, {});

const ControllerOptionsDefault: ControllerOptions = {
    path: '/',
    middlewares: [],
};
const Controller = (options?: ControllerOptions) => (target: any) =>
    setMetadata(target, CoreDecorator.Controller, { ...ControllerOptionsDefault, ...options });

export { Service, Controller };
