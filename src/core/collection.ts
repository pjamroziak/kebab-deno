import { Type } from './types.ts';

class Collection {
    private readonly _objectTypeMap: Map<string, Type<object>>;

    constructor() {
        this._objectTypeMap = new Map<string, Type<object>>();
    }

    public register(name: string, type: Type<object>): Collection {
        this._objectTypeMap.set(name, type);
        return this;
    }

    public get<T extends object>(type: Type<T> | string): Type<object> | undefined {
        return this._objectTypeMap.get(this.resolveName(type));
    }

    public getAll(): Type<object>[] {
        return Array.from(this._objectTypeMap.values());
    }

    public has<T extends object>(type: Type<T> | string): boolean {
        return this._objectTypeMap.has(this.resolveName(type));
    }

    protected resolveName<T extends object>(type: Type<T> | string): string {
        return typeof type === 'string' ? type : type.name;
    }
}

class InstanceCollection extends Collection {
    private readonly _instances: Map<string, object>;
    constructor() {
        super();
        this._instances = new Map<string, object>();
    }

    public registerInstance<T extends object>(name: string, type: T): InstanceCollection {
        this._instances.set(name, type);
        return this;
    }

    public getInstance<T extends object>(type: Type<T> | string): T | undefined {
        return this._instances.get(this.resolveName(type)) as T | undefined;
    }

    public getAllInstances(): object[] {
        return Array.from(this._instances.values());
    }

    public hasInstance<T extends object>(type: Type<T> | string): boolean {
        return this._instances.has(this.resolveName(type));
    }
}

export { Collection, InstanceCollection };
