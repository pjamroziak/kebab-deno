import { ILogger } from '../../logger/interfaces.ts';
import { LoggerService } from '../../logger/logger.service.ts';
import { getCtorParamTypes } from '../../utils/metadata.ts';
import { Collection } from '../collection.ts';
import { IBuilder } from '../interfaces.ts';
import { ServiceResolver } from './service-resolver.builder.ts';
import { Type } from '../types.ts';

class ControllerResolver {
    private readonly _serviceResolver: ServiceResolver;
    private readonly _controllerCollection: Collection;

    get controllers(): Type<object>[] {
        return this._controllerCollection.getAll();
    }

    constructor(serviceResolver: ServiceResolver, controllerCollection: Collection) {
        this._serviceResolver = serviceResolver;
        this._controllerCollection = controllerCollection;
    }

    public resolve<T extends object>(controller: Type<T>, scopeId?: string): T {
        if (!this._controllerCollection.has(controller)) {
            throw new Error(`Controller ${controller.name} is not registered`);
        }

        return this.createControllerInstance(controller, scopeId);
    }

    private createControllerInstance<T extends object>(Type: Type<T>, scopeId?: string): T {
        const ctorParamTypes = getCtorParamTypes(Type);
        const dependencies = ctorParamTypes.map((paramType) => {
            const service = this._serviceResolver.resolve(paramType, scopeId);
            if (service) {
                return service;
            }

            throw new Error(`Dependency not found for ${paramType.name}`);
        });

        const instance = new Type(...dependencies);

        return instance;
    }
}

class ControllerResolverBuilder implements IBuilder<ControllerResolver> {
    private readonly _controllerCollection: Collection;

    constructor() {
        this._controllerCollection = new Collection();
    }

    build(serviceResolver: ServiceResolver): ControllerResolver {
        return new ControllerResolver(serviceResolver, this._controllerCollection);
    }

    public addController<T extends object>(type: Type<T>): ControllerResolverBuilder {
        if (this._controllerCollection.has(type)) {
            throw new Error(`Controller ${type.name} already registered`);
        }

        this._controllerCollection.register(type.name, type);
        return this;
    }
}

export { ControllerResolver, ControllerResolverBuilder };
