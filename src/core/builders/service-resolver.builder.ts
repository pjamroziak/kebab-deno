import { ILogger } from '../../logger/interfaces.ts';
import { LoggerService } from '../../logger/logger.service.ts';
import { getCtorParamTypes } from '../../utils/metadata.ts';
import { InstanceCollection, Collection } from '../collection.ts';
import { IBuilder } from '../interfaces.ts';
import { Type } from '../types.ts';

class ServiceResolver {
    private readonly _logger: ILogger;

    private readonly _singletonCollection: InstanceCollection;
    private readonly _transientCollection: Collection;
    private readonly _scopedCollection: Collection;

    private readonly _scopes: Map<string, InstanceCollection>;

    constructor(
        singletonCollection: InstanceCollection,
        transientCollection: Collection,
        scopedCollection: Collection,
    ) {
        this._singletonCollection = singletonCollection;
        this._transientCollection = transientCollection;
        this._scopedCollection = scopedCollection;

        this._logger = this.resolve(LoggerService)?.getLogger(ServiceResolver.name);

        this._scopes = new Map<string, InstanceCollection>();
    }

    public createScope(name: string): void {
        this._scopes.set(name, new InstanceCollection());
    }

    public async removeScope(name: string): Promise<void> {
        if (!this._scopes.has(name)) {
            this._logger.warn(`Scope ${name} does not exist.`);
            return;
        }

        const array = Array.from(this._scopes.get(name)!.getAllInstances());
        const promises = await Promise.allSettled(
            array
                .filter((service): service is { dispose: () => Promise<void> } => {
                    return 'dispose' in service && typeof service.dispose === 'function';
                })
                .map(async (service) => {
                    await service.dispose();
                }),
        );

        for (const promise of promises) {
            if (promise.status === 'rejected') {
                this._logger.error(`Error disposing service: ${promise.reason}`);
            }
        }

        this._scopes.delete(name);
    }

    public resolve<T extends object>(type: Type<T>, scopeId?: string): T {
        if (this._singletonCollection.hasInstance(type)) {
            return this._singletonCollection.getInstance(type)!;
        }

        if (this._singletonCollection.has(type)) {
            const instance = this.createTypeInstance(type);

            this._singletonCollection.registerInstance(instance.constructor.name, instance);
            return instance;
        }

        if (this._transientCollection.has(type)) {
            return this.createTypeInstance(type);
        }

        if (scopeId && this._scopedCollection.has(type)) {
            const scope = this._scopes.get(scopeId);
            if (scope) {
                if (scope.hasInstance(type)) {
                    return scope.getInstance(type)!;
                }

                const instance = this.createTypeInstance(type, scopeId);
                scope.registerInstance(instance.constructor.name, instance);
                return instance;
            }
        }

        throw new Error(`Cannot resolve type: ${type.name}`);
    }

    private createTypeInstance<T extends object>(type: Type<T>, scopeId?: string): T {
        const ctorParamTypes = getCtorParamTypes(type);
        const dependencies = ctorParamTypes.map((paramType) => {
            const service = this.resolve(paramType, scopeId);
            if (service) {
                return service;
            }

            throw new Error(`Dependency not found for ${paramType.name}`);
        });

        const instance = new type(...dependencies);

        return instance;
    }
}

class ServiceResolverBuilder implements IBuilder<ServiceResolver> {
    private readonly _singletonCollection: InstanceCollection;
    private readonly _transientCollection: Collection;
    private readonly _scopedCollection: Collection;

    constructor() {
        this._singletonCollection = new InstanceCollection();
        this._transientCollection = new Collection();
        this._scopedCollection = new Collection();
    }

    build(): ServiceResolver {
        return new ServiceResolver(this._singletonCollection, this._transientCollection, this._scopedCollection);
    }

    public addSingleton<T extends object>(type: Type<T>): ServiceResolverBuilder {
        if (this.has(type)) {
            throw new Error(`Type ${type.name} is already registered.`);
        }

        this._singletonCollection.register(type.name, type);
        return this;
    }

    public addTransient<T extends object>(type: Type<T>): ServiceResolverBuilder {
        if (this.has(type)) {
            throw new Error(`Type ${type.name} is already registered.`);
        }

        this._transientCollection.register(type.name, type);
        return this;
    }

    public addScoped<T extends object>(type: Type<T>): ServiceResolverBuilder {
        if (this.has(type)) {
            throw new Error(`Type ${type.name} is already registered.`);
        }

        this._scopedCollection.register(type.name, type);
        return this;
    }

    public addInstance<T extends object>(instance: T): ServiceResolverBuilder {
        this._singletonCollection.registerInstance(instance.constructor.name, instance);
        return this;
    }

    public has(type: Type<object>): boolean {
        return (
            this._singletonCollection.has(type) ||
            this._transientCollection.has(type) ||
            this._scopedCollection.has(type)
        );
    }
}

export { ServiceResolver, ServiceResolverBuilder };
