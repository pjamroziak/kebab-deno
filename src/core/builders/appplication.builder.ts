import { Router } from '../../http/router.ts';
import { HttpServer } from '../../http/server.ts';
import { RequestHandler } from '../../http/types.ts';
import { LoggerService } from '../../logger/logger.service.ts';
import { ControllerResolver, ControllerResolverBuilder } from './controller-resolver.builder.ts';
import { IBuilder } from '../interfaces.ts';
import { ServiceResolver, ServiceResolverBuilder } from './service-resolver.builder.ts';
import { Type } from '../types.ts';
import { ILoggerService } from '../../logger/interfaces.ts';

class Application {
    public static create(func: (builder: ApplicationBuilder) => void): Application {
        const builder = new ApplicationBuilder();
        func(builder);

        return builder.build();
    }

    private readonly _serviceResolver: ServiceResolver;
    private readonly _controllerResolver: ControllerResolver;

    constructor(serviceResolver: ServiceResolver, controllerResolver: ControllerResolver) {
        this._serviceResolver = serviceResolver;
        this._controllerResolver = controllerResolver;
    }

    public listen(port: number): void {
        const loggerService = this._serviceResolver.resolve(LoggerService);

        const router = new Router(loggerService, this._controllerResolver, this._serviceResolver);

        for (const controller of this._controllerResolver.controllers) {
            router.addRoute(controller);
        }

        const server = new HttpServer(loggerService, router, this._serviceResolver);

        server.listen(port);
    }
}

class ApplicationBuilder implements IBuilder<Application> {
    private _loggerService: LoggerService;

    private readonly _serviceResolverBuilder: ServiceResolverBuilder;
    private readonly _controllersResolverBuilder: ControllerResolverBuilder;

    constructor() {
        this._loggerService = new LoggerService();
        this._serviceResolverBuilder = new ServiceResolverBuilder();
        this._controllersResolverBuilder = new ControllerResolverBuilder();
    }

    public get services(): ServiceResolverBuilder {
        return this._serviceResolverBuilder;
    }

    public get controllers(): ControllerResolverBuilder {
        return this._controllersResolverBuilder;
    }

    public build(): Application {
        const serviceResolver = this._serviceResolverBuilder.build();
        const controllerResolver = this._controllersResolverBuilder.build(serviceResolver);

        return new Application(serviceResolver, controllerResolver);
    }

    public configureServices(func: (builder: ServiceResolverBuilder) => void): ApplicationBuilder {
        func(this._serviceResolverBuilder);
        return this;
    }

    public configureControllers(func: (builder: ControllerResolverBuilder) => void): ApplicationBuilder {
        func(this._controllersResolverBuilder);
        return this;
    }

    public useLoggerService(loggerService: Type<LoggerService>): ApplicationBuilder {
        this._serviceResolverBuilder.addSingleton(loggerService);
        return this;
    }
}

export { Application, ApplicationBuilder };
