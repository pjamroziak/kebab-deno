type Step<I = any, O = any> = (input: I) => O | Promise<O> | Promise<void> | void;

class Pipeline {
    private readonly _steps: Step[];

    constructor() {
        this._steps = [];
    }

    public addStep<I = any, O = any>(...step: Step<I, O>[]): Pipeline {
        this._steps.push(...step);
        return this;
    }

    async run<I, O>(input: I): Promise<O> {
        for (let i = 0; i < this._steps.length; i++) {
            const step = this._steps[i];
            
            const output = await step(input);

            if (i === this._steps.length - 1) {
                return output;
            }
        }
        
        throw new Error('Pipeline is empty');
    }
}

export { Pipeline, type Step };