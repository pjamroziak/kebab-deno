import { createMetadataKey } from '../utils/metadata.ts';
import { Enum } from './types.ts';

const CoreDecorator = {
    Service: createMetadataKey('service'),
    Controller: createMetadataKey('controller'),
}

type CoreDecorator = Enum<typeof CoreDecorator>;

export { CoreDecorator };