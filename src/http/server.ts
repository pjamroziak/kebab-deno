import { ScopesCollectionBuilder } from '../core/scope-collection.builder.ts';
import { ServiceResolver } from '../core/builders/service-resolver.builder.ts';
import { ILogger } from '../logger/interfaces.ts';
import { LoggerService } from '../logger/logger.service.ts';
import { Router } from './router.ts';
import { Context } from './types.ts';

class HttpServer {
    private readonly _logger: ILogger;
    private readonly _router: Router;
    private readonly _services: ServiceResolver;

    private _defaultResponse: Response;

    private _server?: Deno.HttpServer<Deno.NetAddr>;

    constructor(loggerService: LoggerService, router: Router, services: ServiceResolver) {
        this._logger = loggerService.getLogger(HttpServer.name);
        this._router = router;
        this._services = services;

        this._defaultResponse = new Response('Internal Server Error', { status: 500 });
    }

    public setDefaultResponse(response: Response): void {
        this._defaultResponse = response;
    }

    public listen(port: number): Deno.HttpServer<Deno.NetAddr> {
        this._server = Deno.serve(
            {
                port,
                onListen: (localAddr) => {
                    this._logger.info(`Server started on http://${localAddr.hostname}:${localAddr.port}`);
                },
            },
            this.createServeHandler(),
        );
        return this._server;
    }

    private createServeHandler(): Deno.ServeHandler {
        return async (request: Request): Promise<Response> => {
            const url = new URL(request.url);
            const path = url.pathname;
            const method = request.method;

            const context: Context = Object.create({
                id: crypto.randomUUID(),
                body: request.body,
                headers: [...request.headers.entries()].reduce((acc, [key, value]) => {
                    acc[key] = value;
                    return acc;
                }, {} as Record<string, string>),
                request,
            });

            const route = this._router.findRoute(path, method);

            if (!route) {
                return this._defaultResponse;
            }

            this._services.createScope(context.id);

            // this._logger.debug(`Request ${context.id} - ${method}: ${route.pathname}`);
            const response = await route.pipeline.run<Context, any>(context);

            this._services.removeScope(context.id);

            if (this.isResponse(response)) {
                return response;
            }

            if (this.isJsonObj(response)) {
                return new Response(JSON.stringify(response), {
                    status: 200,
                    headers: { 'Content-Type': 'application/json' },
                });
            }

            if (this.isText(response)) {
                return new Response(response);
            }

            return this._defaultResponse;
        };
    }

    private isJsonObj(obj: unknown): boolean {
        return typeof obj === 'object' && obj !== null;
    }

    private isText(obj: unknown): boolean {
        return typeof obj === 'string' || typeof obj === 'number' || typeof obj === 'boolean';
    }

    private isResponse(obj: unknown): obj is Response {
        return obj instanceof Response;
    }
}

export { HttpServer };
