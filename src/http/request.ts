class Req implements Request {
    private readonly _org: Request;

    private readonly _headers: Headers; 

    constructor(org: Request) {
        this._org = org;
    }

	cache: RequestCache;
	credentials: RequestCredentials;
	destination: RequestDestination;
	headers: Headers;
	integrity: string;
	isHistoryNavigation: boolean;
	isReloadNavigation: boolean;
	keepalive: boolean;
	method: string;
	mode: RequestMode;
	redirect: RequestRedirect;
	referrer: string;
	referrerPolicy: ReferrerPolicy;
	signal: AbortSignal;
	url: string;
	clone(): Request {
		throw new Error('Method not implemented.');
	}
	body: ReadableStream<Uint8Array> | null;
	bodyUsed: boolean;
	arrayBuffer(): Promise<ArrayBuffer> {
		throw new Error('Method not implemented.');
	}
	blob(): Promise<Blob> {
		throw new Error('Method not implemented.');
	}
	bytes(): Promise<Uint8Array> {
		throw new Error('Method not implemented.');
	}
	formData(): Promise<FormData> {
		throw new Error('Method not implemented.');
	}
	json(): Promise<any> {
		throw new Error('Method not implemented.');
	}
	text(): Promise<string> {
		throw new Error('Method not implemented.');
	}
    
}