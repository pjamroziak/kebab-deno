import { Pipeline } from '../core/pipe.ts';
import { HttpMethod } from './methods.enum.ts';

class Route {
    private readonly _pathname: string;
    private readonly _method: HttpMethod;
    private readonly _pipeline: Pipeline;

    constructor(pathname: string, method: HttpMethod) {
        this._pipeline = new Pipeline();
        this._pathname = pathname;
        this._method = method;
    }

    public get pathname(): string {
        return this._pathname;
    }

    public get method(): HttpMethod {
        return this._method;
    }

    public get pipeline(): Pipeline {
        return this._pipeline;
    }
}

export { Route };
