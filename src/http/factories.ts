import { ControllerResolver } from '../core/builders/controller-resolver.builder.ts';
import { ServiceResolver } from '../core/builders/service-resolver.builder.ts';
import { Type } from '../core/types.ts';
import { Context, RequestHandler } from './types.ts';

const createMiddlewareEntryPointFactory =
    (builder: ServiceResolver, middleware: Type<any>): RequestHandler =>
    (ctx: Context) => {
        const instance = builder.resolve(middleware, ctx.id);
        return instance.handle(ctx);
    };

const createControllerEntryPointFactory =
    (builder: ControllerResolver, controller: Type<any>, methodName: string | symbol): RequestHandler =>
    (ctx: Context) => {
        const instance = builder.resolve(controller, ctx.id);
        return instance[methodName](ctx);
    };

export { createMiddlewareEntryPointFactory, createControllerEntryPointFactory };
