import { ControllerResolver } from '../core/builders/controller-resolver.builder.ts';
import { CoreDecorator } from '../core/enums.ts';
import { IMiddleware } from '../core/interfaces.ts';
import { ServiceResolver } from '../core/builders/service-resolver.builder.ts';
import { Type, ControllerOptions } from '../core/types.ts';
import { ILogger } from '../logger/interfaces.ts';
import { LoggerService } from '../logger/logger.service.ts';
import { getMetadata } from '../utils/metadata.ts';
import { RoutesMetadataKey } from './constants.ts';
import { createControllerEntryPointFactory, createMiddlewareEntryPointFactory } from './factories.ts';
import { Route } from './route.ts';
import { RequestHandler, RouteParams } from './types.ts';

class TrieNode {
    private _children: Map<string, TrieNode>;
    private _route: Route | null;

    get children(): Map<string, TrieNode> {
        return this._children;
    }

    set children(value: Map<string, TrieNode>) {
        this._children = value;
    }

    get route(): Route | null {
        return this._route;
    }

    set route(value: Route | null) {
        this._route = value;
    }

    constructor() {
        this._children = new Map();
        this._route = null;
    }
}

class Router {
    private readonly _logger: ILogger;
    private readonly _routes: Route[];
    private readonly _controllers: ControllerResolver;
    private readonly _services: ServiceResolver;
    private readonly _root: TrieNode;

    constructor(loggerService: LoggerService, controllers: ControllerResolver, services: ServiceResolver) {
        this._logger = loggerService.getLogger(Router.name);
        this._routes = [];
        this._controllers = controllers;
        this._services = services;

        this._root = new TrieNode();
    }

    public get routes(): Route[] {
        return this._routes;
    }

    public addRoute<T extends Record<string | symbol, RequestHandler> | object>(controller: Type<T>): Router {
        const params = getMetadata<ControllerOptions>(controller, CoreDecorator.Controller);
        const routes = getMetadata<RouteParams[]>(controller.prototype, RoutesMetadataKey);

        if (!params || !routes || routes.length === 0) {
            return this;
        }

        for (const route of routes) {
            const { method, path: routePath, propertyKey } = route;
            const pathname = this.createPathname(params?.path, routePath);

            const middlewareFactories = this.mapMiddlewaresToEntryPoints(params.middlewares || []);
            const controllerFactory = createControllerEntryPointFactory(this._controllers, controller, propertyKey);

            const newRoute = new Route(pathname, method);
            newRoute.pipeline.addStep(...middlewareFactories, controllerFactory);

            let node = this._root;
            const parts = pathname.split('/').filter((part) => part !== '');

            for (const part of parts) {
                if (!node.children.has(part)) {
                    node.children.set(part, new TrieNode());
                }

                node = node.children.get(part)!;
            }

            node.route = newRoute;

            this._logger.info(`Registered endpoint - ${method}: ${newRoute.pathname}`);
        }

        return this;
    }

    public findRoute(path: string, method: string): Route | null {
        const parts = path.split('/').filter((part) => part !== '');
        let node = this._root;

        for (const part of parts) {
            if (!node.children.has(part)) {
                return null;
            }

            node = node.children.get(part)!;
        }

        if (node.route && node.route.method === method) {
            return node.route;
        }

        return null;
    }

    private mapMiddlewaresToEntryPoints(middlewares: Type<IMiddleware>[]): RequestHandler[] {
        return middlewares.map((middleware) => createMiddlewareEntryPointFactory(this._services, middleware));
    }

    private createPathname(base?: string, path?: string): string {
        let pathname = '/';

        const baseStartWithSlash = base?.startsWith('/');
        const baseEndsWithSlash = base?.endsWith('/');

        const pathStartWithSlash = path?.startsWith('/');

        pathname += baseStartWithSlash ? base!.slice(1) : base;

        if (pathStartWithSlash) {
            if (baseEndsWithSlash) {
                pathname += path!.slice(1);
            } else {
                pathname += path;
            }
        } else if (path) {
            if (baseEndsWithSlash) {
                pathname += path;
            } else {
                pathname += `/${path}`;
            }
        }

        pathname = pathname.replace(/\/\/+/g, '/');

        if (pathname.endsWith('/')) {
            pathname = pathname.slice(0, -1);
        }

        return pathname;
    }
}

export { Router };
