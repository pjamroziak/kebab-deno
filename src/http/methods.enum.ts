import { Enum } from '../core/types.ts';

const HttpMethod = {
    GET: 'GET',
    POST: 'POST',
    PUT: 'PUT',
    DELETE: 'DELETE',
    PATCH: 'PATCH',
};

type HttpMethod = Enum<typeof HttpMethod>;

export { HttpMethod };
