import { createMetadataKey, getMetadata, setMetadata } from '../utils/metadata.ts';
import { RoutesMetadataKey } from './constants.ts';
import { HttpMethod } from './methods.enum.ts';

function createHttpMethodDecorator(method: string, path?: string): MethodDecorator {
    return function (target: any, propertyKey: string | symbol, _: PropertyDescriptor): void {
        const routes = getMetadata<any>(target, RoutesMetadataKey) || [];
        setMetadata(target, RoutesMetadataKey, [...routes, { method, path, propertyKey }]);
    };
}

const Get = (path?: string) => createHttpMethodDecorator(HttpMethod.GET, path);
const Post = (path?: string) => createHttpMethodDecorator(HttpMethod.POST, path || '/');
const Put = (path?: string) => createHttpMethodDecorator(HttpMethod.PUT, path || '/');
const Delete = (path?: string) => createHttpMethodDecorator(HttpMethod.DELETE, path || '/');
const Patch = (path?: string) => createHttpMethodDecorator(HttpMethod.PATCH, path || '/');

export { createHttpMethodDecorator, Delete, Get, Patch, Post, Put };
