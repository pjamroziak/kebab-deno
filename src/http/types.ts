import { Type } from '../core/types.ts';
import type { HttpMethod } from './methods.enum.ts';

type RequestHandler = (ctx: Context) => Promise<void> | void | Promise<Response> | Response;

type Context<T = any> = {
    id: string;
    request: Request;
    onFinish?: () => void;
};

type RouteParams = {
    method: HttpMethod;
    path: string;
    propertyKey: string | symbol;
};

type Route = {
    controller: Type<any>;
    
};

export type { RequestHandler, Context, RouteParams };
