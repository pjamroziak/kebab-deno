import { createMetadataKey } from '../utils/metadata.ts';

const RoutesMetadataKey = createMetadataKey('routes');

export { RoutesMetadataKey };