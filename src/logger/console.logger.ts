import { LogLevel } from './enums.ts';
import { ILogger } from './interfaces.ts';
import { LogFormatter } from './log.formatter.ts';
import { writeAll } from "@std/io/write-all";

class ConsoleLogger implements ILogger {
    private readonly _context: string;
    private readonly _formatter: LogFormatter;

    constructor(context: string) {
        this._formatter = new LogFormatter();
        this._context = context;
    }

    info(message: string): void {
        const formattedMessage = this._formatter.format(LogLevel.INFO, message, this._context);
        this.log(formattedMessage);
    }
    error(message: string): void {
        const formattedMessage = this._formatter.format(LogLevel.ERROR, message, this._context);
        this.log(formattedMessage);
    }
    warn(message: string): void {
        const formattedMessage = this._formatter.format(LogLevel.WARN, message, this._context);
        this.log(formattedMessage);
    }
    debug(message: string): void {
        const formattedMessage = this._formatter.format(LogLevel.DEBUG, message, this._context);
        this.log(formattedMessage);
    }
    critical(message: string): void {
        const formattedMessage = this._formatter.format(LogLevel.ERROR, message, this._context);
        this.log(formattedMessage);
    }

    private log(message: string): void {
        writeAll(Deno.stdout, new TextEncoder().encode(message));
    }
}

export { ConsoleLogger };
