import { Enum } from '../core/types.ts';

const LogLevel = {
    INFO: 'info',
    WARN: 'warn',
    ERROR: 'error',
    DEBUG: 'debug',
} as const;

const ConsoleTextColor = {
    NoColor: '0',
    White: '37',
    Green: '32',
    Yellow: '33',
    Red: '31',
    Purple: '35',
} as const;

type LogLevel = Enum<typeof LogLevel>;
type ConsoleTextColor = Enum<typeof ConsoleTextColor>;

export { LogLevel, ConsoleTextColor };
