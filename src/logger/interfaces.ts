interface ILoggerService {
    getLogger(ctx: string): ILogger;
}

interface ILogger {
    info(message: string): void;
    error(message: string): void;
    warn(message: string): void;
    debug(message: string): void;
    critical(message: string): void;
}

export type { ILoggerService, ILogger };
