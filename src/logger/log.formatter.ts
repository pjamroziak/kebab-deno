import { ConsoleTextColor, LogLevel } from './enums.ts';

class LogFormatter {
    format(level: LogLevel, message: string, context: string): string {
        const timestamp = this.getTimestamp();
        const levelColor = this.getLevelColor(level);
        const levelString = level.toUpperCase();
        const contextString = context;

        return `\x1b[${levelColor}m[${timestamp}][${levelString}][${contextString}]: \x1b[${ConsoleTextColor.White}m${message}\x1b[0m\n`;
    }

    private getTimestamp(): string {
        const date = new Date();
        return date.toISOString();
    }

    private getLevelColor(level: LogLevel): string {
        switch (level) {
            case LogLevel.INFO:
                return ConsoleTextColor.Green;
            case LogLevel.WARN:
                return ConsoleTextColor.Yellow;
            case LogLevel.ERROR:
                return ConsoleTextColor.Red;
            case LogLevel.DEBUG:
                return ConsoleTextColor.Purple;
            default:
                return ConsoleTextColor.NoColor;
        }
    }
}

export { LogFormatter };
