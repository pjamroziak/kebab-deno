import { Service } from '../core/decorators.ts';
import { ConsoleLogger } from './console.logger.ts';
import { ILogger, ILoggerService } from './interfaces.ts';
import { LogFormatter } from './log.formatter.ts';

@Service()
class LoggerService implements ILoggerService {
    private readonly _logFormatter: LogFormatter;

    constructor() {
        this._logFormatter = new LogFormatter();
    }

    getLogger(ctx: string): ILogger {
        return new ConsoleLogger(this._logFormatter, ctx);
    }
}

export { LoggerService };
