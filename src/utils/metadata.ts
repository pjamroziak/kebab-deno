import { Reflect } from '@dx/reflect';
import { Type } from '../core/types.ts';
import { MetadataPrefix } from '../core/constants.ts';

function setMetadata(
    target: any,
    metadataKey: string | symbol,
    metadataValue: unknown,
    propertyKey?: string | symbol,
): void {
    propertyKey
        ? Reflect.defineMetadata(metadataKey, metadataValue, target, propertyKey)
        : Reflect.defineMetadata(metadataKey, metadataValue, target);
}

function getMetadata<T>(
    target: any,
    metadataKey: string | symbol,
    propertyKey?: string | symbol,
): T | undefined {
    return propertyKey
        ? Reflect.getMetadata(metadataKey, target, propertyKey)
        : Reflect.getMetadata(metadataKey, target);
}

function getCtorParamTypes(target: Type<any>): Type<any>[] {
    const ctorParamTypes = getMetadata<Type<any>[]>(target, 'design:paramtypes') || [];
    return ctorParamTypes;
}

function createMetadataKey(key: string): string {
    return `${MetadataPrefix}:${key}`;
}

export { setMetadata, getMetadata, getCtorParamTypes, createMetadataKey };